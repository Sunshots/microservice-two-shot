# Generated by Django 4.0.3 on 2022-10-20 04:46

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=150)),
                ('model_name', models.CharField(max_length=150)),
                ('color', models.CharField(max_length=100)),
                ('picture_url', models.URLField(blank=True, null=True)),
            ],
        ),
    ]
