import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { renderMatches } from 'react-router-dom';


class HatList extends React.Component{
  constructor(props){
    super(props);
    this.state={
      hats:[]
    }
  }
  async loadhats() {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        this.setState({hats:data.hats})
      } else {
        console.error(response);
      }
    }

  async delete(id){
    await fetch(`http://localhost:8090/api/hats/${id}`, {
            method: 'DELETE',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json'
            }
        }).then(() => {
            let updatedhats = [...this.state.hats].filter(i => i.id !== id);
            this.setState({hats : updatedhats});
        });
  }



  async componentDidMount() {
        this.loadhats()
    }
      render(){
        return(
          <table className="table table-striped">
             <thead>
                 <tr>
                     <th>Style</th>
                     <th>Fabric</th>
                     <th>Color</th>
                     <th></th>
                 </tr>
             </thead>
             {/* propls .hat is coming from top async func */}
             <tbody>
                 {this.state.hats.map(hat =>{
                     return(
                         <tr key={hat.id}>
                             <td>{ hat.style_name }</td>
                             <td>{ hat.fabric }</td>
                             <td>{ hat.color }</td>
                             <button onClick={()=> this.delete(hat.id)} type="button" className="btn btn-light">Delete</button>
                         </tr>
                     )
                 })}
             </tbody>
         </table>
          );
        }
      }


export default HatList;
