import React from 'react';

class ShoeForm extends React.Component{
    constructor(props) {
        super(props)
        this.state = {      
            manufacturer: '',
            model_name: '',
            color: '',
            picture_url: '',
            bins: [],
        };
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePictureUrlChange = this.handlePictureUrlChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this) ;
    };

    async handleSubmit(event) {

        event.preventDefault();
        const data = {...this.state};
        // data is equal to state of the infro I inputted
        // var(this.state) that has info that I want to track
        // state change casues re-render
        console.log(data);
        
        const binUrl = 'http://localhost:8080/api/shoes/';
        delete data.bins
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
  
          const response = await fetch(binUrl, fetchConfig);
          if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);
  
            const cleared = {
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
            };
            this.setState(cleared);
            window.location.href = "http://localhost:3000/list"
          }
      }

      handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
      }
  
      handleModelNameChange(event) {
          const value = event.target.value;
          this.setState({model_name: value})
      }
      
      handleColorChange(event) {
          const value = event.target.value;
          this.setState({color: value})
      }
  
      handlePictureUrlChange(event) {
          const value = event.target.value;
          this.setState({picture_url: value})
      }
  
      handleBinChange(event) {
          const value = event.target.value;
          this.setState({bin: value})
      }
          
      async componentDidMount(props) {
          const url = 'http://localhost:8100/api/bins/';
          const response = await fetch(url);
      
          if (response.ok) {
            const data = await response.json();
            this.setState({bins:data.bins});
  
          }
        }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Shoe</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                      {/* add submit  */}
                  <div className="form-floating mb-3">
                    <input onChange={this.handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                    <label htmlFor="manufacturer">Brand</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleModelNameChange} placeholder="Model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                    <label htmlFor="model_name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="ends" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <textarea onChange={this.handlePictureUrlChange}  placeholder="Picture_url" required type="textaera" name="picture_url" id="picture_url" className="form-control" cols="30" rows="40"></textarea>
                    <label htmlFor="picture_url">Image Url</label>
                  </div>
                  <select onChange={this.handleBinChange} required name="bin" id="bin" className="form-select">
                    <option value="">Choose Bin</option>
                    { this.state.bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                            )
                        })}
    
                  </select>
                  <br/>
                  <button className="btn btn-warning">Create</button>
                </form>
              </div>
            </div>
          </div>
    );
}}
    export default ShoeForm;
    